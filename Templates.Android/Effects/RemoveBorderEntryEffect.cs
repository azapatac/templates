﻿using Android.Graphics.Drawables;
using Templates.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("University")]
[assembly: ExportEffect(typeof(RemoveBorderEntryEffect), "RemoveBorderEntryEffect")]

namespace Templates.Droid.Effects
{
    public class RemoveBorderEntryEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            if (Control != null)
            {
                Control.Background = new ColorDrawable(Android.Graphics.Color.Transparent);
            }
        }

        protected override void OnDetached() { }
    }
}