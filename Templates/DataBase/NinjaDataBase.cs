﻿namespace Templates.DataBase
{
    using System.Collections.Generic;
    using Templates.Models;

    public static class NinjaDataBase
    {
        public static List<Ninja> Ninjas = new List<Ninja>
        {
            { new Ninja { Age = 20, Name = "Momochi" } },
            { new Ninja { Age = 10, Name = "Fujibayashi" } },
            { new Ninja { Age = 16, Name = "Ishikawa" } },
            { new Ninja { Age = 11, Name = "Hattori" } },
            { new Ninja { Age = 15, Name = "Mochizuki" } },
        };

        public static List<Ninja> LoadNinjas()
        {
            return Ninjas;
        }
    }
}