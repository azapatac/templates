﻿namespace Templates.DataTemplateSelector
{
    using Templates.Models;
    using Xamarin.Forms;

    public class NinjaDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate NinjaTemplate { get; set; }
        public DataTemplate OlderNinjaTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((Ninja)item).Age > 13 ? OlderNinjaTemplate : NinjaTemplate;
        }
    }
}