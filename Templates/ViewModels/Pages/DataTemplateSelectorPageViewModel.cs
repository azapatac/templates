﻿namespace Templates.ViewModels.Pages
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Templates.DataBase;
    using Templates.Models;

    public class DataTemplateSelectorPageViewModel : BaseViewModel
    {
        IList<Ninja> ninjas;
        public IList<Ninja> Ninjas
        {
            get
            {
                return ninjas;
            }
            set
            {
                if (value != ninjas)
                {
                    ninjas = value;
                    OnPropertyChanged();
                }
            }
        }

        public DataTemplateSelectorPageViewModel()
        {
            Title = "DataTemplate";
            var _ninjas = NinjaDataBase.LoadNinjas();
            Ninjas = new ObservableCollection<Ninja>(_ninjas);
        }
    }
}