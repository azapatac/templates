﻿namespace Templates.Converters
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using Xamarin.Forms;

    public class ImageResourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return ImageSource.FromResource((string)value, typeof(ImageResourceConverter).GetTypeInfo().Assembly);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}