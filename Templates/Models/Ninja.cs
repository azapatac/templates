﻿namespace Templates.Models
{
    public class Ninja
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
}