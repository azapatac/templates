﻿using Templates.iOS.Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("University")]
[assembly: ExportEffect(typeof(RemoveBorderEntryEffect), "RemoveBorderEntryEffect")]

namespace Templates.iOS.Effects
{
    public class RemoveBorderEntryEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            if (Control != null)
            {
                var textField = (UITextField)Control;
                textField.BorderStyle = UITextBorderStyle.None;
            }
        }

        protected override void OnDetached() { }
    }
}